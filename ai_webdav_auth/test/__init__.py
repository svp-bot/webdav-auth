import logging
import os
import socket
import unittest
from ldap_test import LdapServer


# Attributes of the 'test-user.ldif' LDAP fixture.
TEST_DN = 'ftpname=testdav,uid=test@investici.org,ou=People,dc=investici,dc=org,o=Anarchy'
TEST_FTPNAME = 'testdav'
TEST_UID = 19475
TEST_PASS = 'password'
TEST_HOST = 'latitanza'


class LdapTestBase(unittest.TestCase):

    LDIFS = []

    @classmethod
    def setup_class(cls):
        # Start the local LDAP server.
        cls.ldap_port = free_port()
        cls.ldap_password = 'testpass'
        ldifs = [os.path.join(os.path.dirname(__file__), 'fixtures', x)
                 for x in ['base.ldif'] + cls.LDIFS]
        logging.getLogger('py4j.java_gateway').setLevel(logging.ERROR)
        cls.server = LdapServer({
            'port': cls.ldap_port,
            'bind_dn': 'cn=manager,o=Anarchy',
            'password': cls.ldap_password,
            'base': {
                'objectclass': ['organization'],
                'dn': 'o=Anarchy',
                'attributes': {'o': 'Anarchy'},
            },
            'ldifs': ldifs,
        })
        cls.server.start()

    @classmethod
    def teardown_class(cls):
        cls.server.stop()


def free_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    s.bind(('', 0))
    port = s.getsockname()[1]
    s.close()
    return port


class StubBase(object):

    def setUp(self):
        self._stubs = []
        super(StubBase, self).setUp()

    def tearDown(self):
        super(StubBase, self).tearDown()
        for baseref, attr, fn in self._stubs:
            setattr(baseref, attr, fn)

    def stub(self, baseref, attr, fn):
        oldfn = getattr(baseref, attr)
        self._stubs.append((baseref, attr, oldfn))
        setattr(baseref, attr, fn)
