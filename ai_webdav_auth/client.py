import json
import socket


class DavAuthClient(object):

    def __init__(self, socketpath):
        self.socketpath = socketpath
        self._accounts = None

    def _do_request(self, request):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            sock.connect(self.socketpath)
            sock.sendall(json.dumps(request).encode('utf-8'))
            sock.shutdown(socket.SHUT_WR)
            return json.load(sock.makefile('r'))
        finally:
            sock.close()

    def authenticate(self, dn, password):
        return self._do_request({
            'type': 'auth', 'dn': dn, 'password': password})

    def get_accounts(self):
        # Cache the account map.
        if self._accounts is None:
            self._accounts = self._do_request({'type': 'get_accounts'})
        return self._accounts
