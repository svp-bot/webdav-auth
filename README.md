webdav-auth
===

The authentication server bridges the [users-dav](https://git.autistici.org/ai3/docker/apache2-users-dav)
container (which runs the [webdav-server](https://git.autistici.org/ai3/tools/webdav-server) component)
and the local [auth-server](https://git.autistici.org/id/auth).

It runs as a system-level daemon.

